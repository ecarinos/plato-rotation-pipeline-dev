import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

'''
Collection of functions dedicated to write and
manage PLATO-required outputs of the demonstrator.
'''

def write_output (filename, dp_name, dp) :
  '''
  Write a data product into a csv file. 
  '''
  return
