ROOSTER
#######

.. autoclass:: plato_msap4_demonstrator.ROOSTER
	:special-members: __init__
	:members: 

.. autofunction:: plato_msap4_demonstrator.create_rooster_feature_inputs

