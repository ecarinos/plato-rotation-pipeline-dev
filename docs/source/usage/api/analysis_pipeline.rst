Analysis pipeline
#################

.. automodule:: plato_msap4_demonstrator

.. autofunction:: plato_msap4_demonstrator.analysis_pipeline


Lomb-Scargle periodogram
************************

.. autofunction:: plato_msap4_demonstrator.compute_lomb_scargle

.. autofunction:: plato_msap4_demonstrator.compute_prot_err_gaussian_fit_chi2_distribution

.. autofunction:: plato_msap4_demonstrator.plot_ls


Auto-correlation function (ACF)
*******************************

.. autofunction:: plato_msap4_demonstrator.compute_acf

.. autofunction:: plato_msap4_demonstrator.find_period_acf

.. autofunction:: plato_msap4_demonstrator.plot_acf


Composite spectrum (CS)
***********************

.. autofunction:: plato_msap4_demonstrator.compute_cs

.. autofunction:: plato_msap4_demonstrator.compute_prot_err_gaussian_fit

.. autofunction:: plato_msap4_demonstrator.plot_cs

Photometric index (Sph)
***********************

.. autofunction:: plato_msap4_demonstrator.compute_sph

.. autofunction:: plato_msap4_demonstrator.compute_lomb_scargle_sph

Wavelet analysis
****************

.. autofunction:: plato_msap4_demonstrator.compute_wps

.. autofunction:: plato_msap4_demonstrator.plot_wps

