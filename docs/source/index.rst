.. plato_msap4_demonstrator documentation master file, created by
   sphinx-quickstart on Mon Oct 17 10:42:52 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PLATO MSAP4 demonstrator: Rotation and activity
====================================================

Welcome to the documentation of the `PLATO <https://platomission.com/>`_ Module for Stellar AstroPhysics 4 (MSAP4) rotation and activity demonstrator module. The module provides functions and notebooks executing the tasks that will be implemented in the PLATO Stellar Analysis System. 


The demonstrator is developed at the `Osservatorio astrofisico di Catania <https://www.oact.inaf.it/>`_ (INAF-OACT).

Contact address: sylvain.breton@inaf.it

.. toctree::
   :maxdepth: 1
   :caption: User guide

   usage/installation/installation
   usage/fourier_analysis/fourier_analysis
   usage/timeseries_analysis/timeseries_analysis
   usage/rooster_training_framework/rooster_training_framework
   usage/stellar_analysis_framework/stellar_analysis_framework
   usage/cs_rooster_sph_analysis/cs_rooster_sph_analysis
   usage/cycle_determination/cycle_determination
   usage/wavelet_analysis/wavelet_analysis


.. toctree::
   :maxdepth: 2
   :caption: Detailed API

   usage/api/analysis_pipeline
   usage/api/rooster
